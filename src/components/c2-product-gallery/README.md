# C2 Product Gallery
### Overview
Product gallery with a thumbnail image slider. Thumbnails are selected and update the main feature view. Videos autoplay when selected form thumbnail.

### Props
| Props             | Type      | Default |
| ----------------- | --------- | ------- |
| thumbnails        | Array     | 1       |
| autoplayVideos    | Boolean   | true    |

### Slots
| Name              |
| ----------------- |
| play-icon         |

### Code snippet
```
    <c2-product-gallery 
        :thumbnails="thumbnailsList"
        :autoplay-videos="false"
    >
        <template slot="play-icon">
            <play-outline-filled-icon :size="svgSize" />
        </template>
    </c2-product-gallery>
```

### Data snippet
```
    [
        {
            alt: 'Product Image',
            featuredSrc: '/images/thumbnail.png',
            thumbnailSrc: '/images/product-image.png',
            type: null,

        },
        {
            alt: 'Video alt text',
            thumbnailSrc: '/images/video-thumbnail.png',
            featuredSrc: '/videos/video.mp4',
            type: 'mp4'
        }
    ]
```
