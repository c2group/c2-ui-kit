# C2 Grid Component

### Container Props
| Props                | Type        | Default |
| -------------------- | ----------- | ------- |
| className            | String      | null    |
| maxWidth             | String      | 1400px  |
| containerGutterWidth | Number      | 16      |

### Row Props
| Props          | Type        | Default | Options            |
| -------------- | ----------- | ------- |                    |
| className      | String      | null    |                    |
| rowGutterWidth | Number      | 16      |                    |
| justifyContent | String      | null    | start, center, end |
| alignItems     | String      | null    | start, center, end |

### Column Props
| Props             | Type        | Default | Options |
| ----------------- | ----------- | ------- |         |
| className         | String      | null    |         |
| columnGutterWidth | Number      | 16      |         |
| size              | Number      | 1-12    |         |
| sm                | Number      | 1-12    | 1-12    |
| md                | Number      | 1-12    | 1-12    |
| lg                | Number      | 1-12    | 1-12    |
| xlg               | Number      | 1-12    | 1-12    |
| offsetSize        | Number      | 1-11    | 1-11    |
| offsetSm          | Number      | 1-11    | 1-11    |
| offsetMd          | Number      | 1-11    | 1-11    |
| offsetLg          | Number      | 1-11    | 1-11    |
| offsetXlg         | Number      | 1-11    | 1-11    |

### Code snippet
```
<c2-container
    className="custom-container-class"
    :containerGutterWidth="24">
    <c2-row
        className="custom-row-class"
        justifyContent="center"
        alignItems="start"
        :rowGutterWidth="24">
        <c2-column
            className="custom-column-class"
            :columnGutterWidth="24"
            :offsetSize="2"
            :offsetSm="2"
            :size="12"
            :lg="4">
            Content inside grid here...
        </c2-column>
    </c2-row>
</c2-container>
```
