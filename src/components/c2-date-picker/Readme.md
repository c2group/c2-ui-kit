# C2 Date Picker Component
### Overview
Pikaday Date Picker (https://github.com/Pikaday/Pikaday)

### Props
| Props                  | Type        | Default                    | Valid Values               |
| ---------------------- | ----------- | -------------------------- | -------------------------- | 
| calendarOptions        | Object      | \{format: 'M/D/YYYY'\}     | see GitHub for all values  |
| dateLabel              | String      | Date                       |                            |
| disabledClass          | String      | ''                         |                            |
| errorClass             | String      | ''                         |                            |
| hasError               | Boolean     | false                      |                            |
| hideInput              | Boolean     | false                      |                            |
| hideLabel              | Boolean     | false                      |                            |
| invalidMessage         | String      | Date is invalid            |                            |
| isDisabled             | Boolean     | false                      |                            |
| isRequired             | Boolean     | false                      |                            |
| patternValidator       | String      | \\\d\{1,2\}/\\\d\{1,2\}/\\\d\{4\}|                            |
| placeholder            | String      | mm/dd/yyyy                 |                            |
| successClass           | String      | ''                         |                            |
| value                  | String,Date | ''                         |                            |

### Events
Component emits 'change' event with values from Date Picker input:
```
this.$emit('change', this.dataValue);
```

### Slots
calendar-icon,
error-icon,
success-icon

### Code snippet
```
<c2-datepicker
    :calendar-options="{ numberOfMonths: 3 }"
    :date-label="'Choose a date:'"
    :disabled-class="'disabled-date'"
    :error-class="'error-date'"
    :has-error="false"
    :hide-input="false"
    :hide-label="false"
    :invalid-message="'Please choose a valid date.'"
    :is-disabled="false"
    :is-required="false"
    :pattern-validator="'\\d{1,2}/\\d{1,2}/\\d{4}'"
    :place-holder="'mm/dd/yyyy'"
    :success-class="'success-date'"
    :value="'1/1/2021'"
>
    <!-- Use the 'calendar-icon' Slot -->
    <template v-slot:calendar-icon>
        <ClockIcon />
    </template>

    <!-- Use the 'error-icon' Slot -->
    <template v-slot:error-icon>
        <YieldIcon />
    </template>

    <!-- Use the 'success-icon' Slot -->
    <template v-slot:success-icon>
        <CheckmarkIcon />
    </template> 
</c2-datepicker>

<style lang="scss" scoped>
    .disabled-date {
        color: #dddddd;
        cursor: not-allowed;
    }

    .error-date {
        color: #ff0000;
    }

    .success-date {
        color: #00ff00;
    }
</style>
```