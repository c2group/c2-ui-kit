# C2 Text Input
Easy to use input field that includes validation, validation messaging, labels, success messing and more built in props.

### Select Props
| Props                 | Type              | Default   |
| --------------------- | ----------------- | --------- |
| erroClass             | String            |           |
| errorMessage          | String            | null      |
| disabledClass         | String            |           |
| hideLabel             | Boolean           | false     |
| isDisabled            | Boolean           | false     |
| isInvalid             | Boolean           | false     |
| isValid               | Boolean           | false     |
| isRequired            | Boolean           | false     |
| label                 | String            | null      |
| regexValidator        | RegExp            | null      |
| successClass          | String            |           |
| type                  | String            | 'text'    |
| value                 | [String, Number]  | null      |
| desktopPlaceholder    | String            | null      |
| mobilePlaceholder     | String            | null      |


### Code snippet
```
    <c2-text-input 
        v-model="name"
        :label="mdoel.label"
        :has-error="errorStatus"
        :placeholder="model.placeholder"
    />
```
