# C2 Hero Banner Component
### Overview
C2 Hero that allows you to pass an image, and position that will determine if Content is inside or below the image.

### Props
| Props                  | Type        | Default     | Valid Values               |
| ---------------------- | ----------- | ----------- | -------------------------- | 
| backgroundImage        | String      | Null        |                            |
| backgroundImageMobile  | String      | Null        |                            |
| backgroundColor        | String      | Null        | hex color                  |
| backgroundOverlay      | String      | Null        | rgba color                 |
| contentPosition        | String      | inside      | inside,below               |
| maxHeight              | String      | null        | px,rem,em,%                |
| mobileMediaBreakpoint  | String      | 420px       |                            |

### Slots
Default unamed slot

### Code snippet
```
<c2-hero-banner 
    :background-color="'#fff'"
    :background-image="model.desktopImageUrl"
    :background-image-mobile="model.mobileImageUrl"
    :background-overlay="'rgba(0,0,0,0.1)'"
    :content-position="below"
    :max-height="'300px'"
    :mobile-media-breakpoint="'767px'"
>
    <!-- Content to populate the slot goes here -->
</c2-hero-banner>
```