import C2HeroBanner from './c2-hero-banner.vue';
import C2ImageHeroBanner from './_c2-image-hero-banner.vue';
import C2BackgroundColorHeroBanner from './_c2-background-color-hero-banner.vue';

export { C2HeroBanner, C2ImageHeroBanner, C2BackgroundColorHeroBanner };
