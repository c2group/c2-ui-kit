# C2 Radio Input
### Overview
C2 radio input that creates a form radio element.

### Props
| Props        | Type        |
| ------------ | ----------- |
| disabledClass  | String    |
| isDisabled  | Boolean      |
| label  | String            |
| name  | String (required)  |
| modelValue  | Boolean      |
| value  | [String, Boolean] |

### Slots
| Name        |
| ----------- |
| radio-icon |

### Code snippet
```
<c2-radio-input
	:disabledClass="disabled"
	:isDisabled="false"
	:label="model.label"
	:modelValue="model.modelValue"
	:name="model.Name"
	:value="model.initalValue"
>
	<inline-svg slot="radio-icon" src="/Static/dist/svg/radio-icon.svg" />
</c2-radio-input>
```
