# C2 Slider
### Overview
Dynamic horizontal slider that can hold cards in it and slides them based on the slide to show amount. Slide speed, indicators, overflow, nav alignment and gutter options available. Default slides to show set will be used on desktop, tablet and mobile. You can also define a tablet and mobile amount if needed.

### Props
| Props					| Type		| Default	|
| --------------------- | ---------- | -------- |
| slideSpeed			| Number	| 300		|
| slidesToShow			| Number	| 1			|
| slidesToShowTablet	| Number	| null		|
| slidesToShowMobile	| Number	| null		|
| gutter				| Number	| 0			|
| hideIndicators		| Boolean	| true		|
| navAlignment			| String	| 'right'	|
| showOverflow			| Boolean	| false		|

### Slots
| Name			|
| ------------- |
| slide			|
| extraButton	|
| previousIcon	|
| previousText	|
| nextIcon		|
| nextText		|

### Code snippet
```
<c2-slider
	v-if="carouselActivated && model.linkCardItems && model.linkCardItems.length > 0"
	:slides-to-show="3"
	:slides-to-show-tablet="2"
	:slides-to-show-mobile="1"
	:slide-speed="500"
	:gutter="12"
	:show-overflow="true"
	:hide-indicators="false"
>
	<c2-card
		v-for="(alertItem, index) in model.cardItems"
		:key="index"
		slot="slide"
	>
		...Card HTML here
	</c2-card>

	<span class="button-label sr-only" slot="previousText">
		Previous Slide
	</span>
	<span class="button-icon" slot="previousIcon">
		<inline-svg :keep-during-loading="false" src="/Static/dist/svg/arrow-left.svg" />
	</span>

	<span class="button-label sr-only" slot="nextText">
		Next Slide
	</span>
	<span class="button-icon" slot="nextIcon">
		<inline-svg :keep-during-loading="false" src="/Static/dist/svg/arrow-right.svg" />
	</span>
</c2-slider>
```
