# C2 Card Component

### Props
| Props        | Type        |  Options    |
| ------------ | ----------- | ----------- |
| href         | String      |             |

### Slots
| Name        |
| ----------- |
| header      |
| body        |

### Code snippet
```
<c2-card
    v-if="isCard"
    :href="url"
>
    <template slot="header">
        Card Header
    </template>
    <template slot="body">
        Card Body
    </template>
</c2-card>
```
