import C2List from './c2-list.vue';
import C2ListItem from './c2-list-item.vue';

export { C2List, C2ListItem };
