# C2 List
### Overview
C2 List is a ul list wrapper component that applies horizontal/vertical flex alignment options for developers. 

### Props
| Props         | Type        | Default     |
| ------------- | ----------- | ----------- |
| alignment     | String      | vertical (horizontal) |
| ordered       | Boolean     | false       |
| listStyle     | String      | disc (none, circle, square, disc, decimal) |

### Code snippet
```
    <c2-list
        alignment="horizontal"
    >
        <c2-list-item></c2-list-item>
    </c2-list>
```

# C2 List Item
### Overview
C2 List Item is a child component of C2 List. Optional disabled list items and class names allowed

### Props
| Props         | Type        | Default     |
| ------------- | ----------- | ----------- |
| isDisabled    | Boolean     | false       |
| activeClass   | String      |             |

### Code snippet
```
    <c2-list-item
        v-for="(item, index) in model.itemsList"
        :key="index"
    >
        {{ item.name }}
    </c2-list-item>
```
