import C2Breadcrumbs from './c2-breadcrumbs.vue';
import C2BreadcrumbItem from './c2-breadcrumb-item.vue';

export { C2Breadcrumbs, C2BreadcrumbItem };