# C2 Breadcrumbs Component

### Props
| Props        | Type        |
| ------------ | ----------- |
| ariaLabel    | String      |

### Code snippet
```
<c2-breadcrumbs ariaLabel="breadcrumbs" class="breadcrumb-items">
    <c2-breadcrumb-item
        v-for="(breadcrumb, index) in breadcrumbs"
        v-if="breadcrumb.displayInBreadcrumbs || index === breadcrumbs.length - 1"
        :key="`breadcrumb-${index}`"
        :is-home="index === 0"
        :is-current="index === breadcrumbs.length - 1"
        :href="breadcrumb.url"
        target="_self"
    >
        <span slot="text">{{ breadcrumb.pageName }}</span>
    </c2-breadcrumb-item>
</c2-breadcrumbs>
```

# C2 Breadcrumb Item Component

### Props
| Props        | Type        |
| ------------ | ----------- |
| currentClass | String      |
| isCurrent    | Boolean     |
| isHome       | Boolean     |
| href         | String      |
| target       | String      |