# C2 Text Area
Easy to use textarea field that includes validation, validation messaging, labels, success messing and more built in props.

### Select Props
| Props                 | Type              | Default   |
| --------------------- | ----------------- | --------- |
| erroClass             | String            |           |
| errorMessage          | String            |           |
| disabledClass         | String            |           |
| isDisabled            | Boolean           | false     |
| isInvalid             | Boolean           | false     |
| isValid               | Boolean           | false     |
| isRequired            | Boolean           | false     |
| label                 | String            |           |
| regexValidator        | RegExp            | null      |
| successClass          | String            |           |
| value                 | [String, Number]  | null      |
| desktopPlaceholder    | String            | null      |
| mobilePlaceholder     | String            | null      |

### Code snippet
```
<c2-text-area
    v-model="model.text.value"
    :mobile-placeholder="model.placeholder"
    :desktop-placeholder="model.placeholder"
    :is-invalid="model.text.isInvalid"
    :is-required="model.text.isRequired"
    error-message="This field is required"
    name="text"
    @blur="validateField"
/>
```
