import C2Tab from './c2-tabs.vue';
import C2TabItem from './c2-tab-item.vue';

export { C2Tab, C2TabItem };
