# C2 Image
### Overview
C2 Image is a shortcut for writing images using html5's picture element. This feature uses source set to allow for mobile and desktop image swapping as well as configurating an image aspectRatio.

### Props
| Props                 | Type        | Default     |
| --------------------- | ----------- | ----------- |
| dontLazyLoad          | Boolean     | false       |
| height                | Number      |             |
| width                 | Number      |             |
| srcset                | String      |             |
| sizes                 | String      |             |
| alt                   | String      |             |
| aspectRatio           | String      | '1/1' || '16/9' || '7/4' || '3/2' || '4/3' || '2/3' || '3/4' |
| errorImage            | String      |             |
| restrictMaxDimensions | Boolean     | false       |
| src                   | String      |             |
| mobileMedia           | String      |             |
| mobileSrc             | String      |             |


### Code snippet
```
    <c2-image
        :alt="Image Alt ttext"
        mobile-src="/Static/img/mobile.webp"
        src="/Static/img/desktop.webp"
        aspect-ratio="16/9"
    >
```
