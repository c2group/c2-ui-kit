# C2 Loading Bar
### Overview
Loading bar with color, style, percentage and indeterminate options

### Props
| Props             | Type        | Default     |
| ----------------- | ----------- | ----------- |
| color             | String      | #38b2ac     |
| indeterminate     | Boolean     | false       |
| percentage        | Number      | 0           |
| rounded           | Boolean     | true        |

### Code snippet
```
    <c2-loading-bar 
        :rounded="false" 
        :color="loadingBarColor" 
        indeterminate
    />
```