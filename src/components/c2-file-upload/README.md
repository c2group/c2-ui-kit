# C2 File Upload
### Overview
C2 File Upload is a componet that can be used inside the c2 form and organized in a c2 form group. This allows users to upload a file into a form. The uplaoder validates acceptable file types and shows messages based on validation.

### Props
| Props             | Type        | Default         |
| ----------------- | ----------- | --------------- |
| buttonAlignment   | String      | right           |
| buttonLabel       | String      | Browse          |
| disabledClass     | String      |                 |
| errorClass        | String      |                 |
| files             | Array       |                 |
| headerLabel       | String      |                 |
| isDisabled        | Boolean     | false           |
| isInvalid         | Boolean     | false           |
| isValid           | Boolean     | false           |
| isRequired        | Boolean     | false           |
| placeholder       | String      | Upload a file   |
| successClass      | String      |                 |

### Slots
| Name        |
| ----------- |
| error-icon  |

### Code snippet
```
    <c2-file-upload
        :placeholder="uploadPlaceholderText"
        :files="uploadFile"
        v-model="uploadFile"
        accept=".pdf"
    >
    </c2-file-upload> 
```
