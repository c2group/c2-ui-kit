# C2 Site Alert
### Overview
Alert messaging that allows a title and message with closing functionality

### Props
| Props        | Type		| Default						| Required	|
| ------------ | ---------- | ----------------------------- | --------- |
| alertActive  | Boolean	| False							|			|
| alertTitle   | String		|								| True		|
| alertType	   | String		| 'info' ('waring', 'danger') 	| True		|

### Code snippet
```
<c2-site-alert
	alert-type="info"
	:alert-active="true"
	:alert-title="model.informationTitle"
>
	{{ alertMessage }}
</c2-site-alert>
```
