# C2 Video
Easy to use input field that includes validation, validation messaging, labels, success messing and more built in props.

### Props
| Props                 | Type              | Default                                           | Required  |
| --------------------- | ----------------- | ------------------------------------------------- | --------- |
| aspectRatio           | String            | 16/9 ('1/1', '16/9', '3/2', '4/3', '2/3', '3/4')  |           |
| videoSrc              | String            |                                                   | true      |
| videoTitle            | String            |                                                   |           |


### Code snippet
```
    <c2-video
        :aspect-ratio="aspectRatio"
        :video-src="`https://www.youtube.com/embed/${model.viewModel.videoId}`"
        :video-title="model.viewModel.heading"
    />
```
