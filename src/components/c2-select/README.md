# C2 Select
Select input that allows for the standard options loop and group option loop. Error, Invalid and helper messages included. Label boolean for hiding and showing available.

### Select Props
| Props                 | Type        | Default | Required  |
| --------------------- | ----------- | ------- | --------- |
| errorMessage          | String      | null    |           |
| helperMessage         | String      | null    |           |
| hideLabel             | Boolean     |         |           |
| invalidMessage        | String      | null    |           |
| label                 | String      |         |           |
| value                 | String      | null    |           |

### Code snippet using selection option
```
<c2-select-option value="">Sort By</c2-select-option>
<c2-select-option
    v-for="(sortByOpt, index) in sortByOptions"
    :key="`sortByOpt-${index}`"
    :value="sortByOpt"
>
    {{ sortByOpt }}
</c2-select-option>
```

### Code snippet using select option group
```
<c2-select-option value="">Sort By</c2-select-option>
<c2-select-optgroup
    v-for="(sortByOpt, index) in sortByOptions"
    :key="`sortByOpt-${index}`"
    :value="sortByOpt"
>
    {{ sortByOpt }}
</c2-select-optgroup>
```