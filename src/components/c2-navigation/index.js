import C2PrimaryNav from './c2-primary-nav.vue';
import C2StaticNav from './c2-static-nav.vue';

export { C2PrimaryNav, C2StaticNav };
