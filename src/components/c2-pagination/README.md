# C2 Pagination
### Overview
Pagination that has next, previous and go to methods. Set the total pages, default start page and max pages visible

### Props
| Props             | Type        | Default |
| ----------------- | ----------- | ------- |
| currentPage       | Number      | 1       |
| maxPagesVisible   | Number      | 7       |
| totalPages        | Number      | true    |

### Slots
| Name              |
| ----------------- |
| previousPageIcon  |
| nextPageIcon      |

### Code snippet
```
    <c2-pagination
        :max-pages-visible="maxPagesVisible"
        :current-page="currentPage"
        :total-pages="totalPages"
        @change="setPage"
    />
```
