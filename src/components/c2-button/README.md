# C2 Button Component
### Overview
Common button with built-in properties

### Props
| Props        | Type        |  Options    |
| ------------ | ----------- | ----------- |
| Header       | String      |             |
| hasHoverIcon | Boolean     |             |
| hasIcon      | Boolean     |             |
| iconDir      | String      | left,right  |
| isDisabled   | Boolean     |             |

### Slots
| Name        |
| ----------- |
| text        |
| icon        |

### Code snippet
```
<c2-button
    btnClass="btn-primary"
    :hasIcon="true"
    iconDir="left"
    :isDisabled="true"
    @click="buttonAlert">
    <span slot="text">Button</span>
    <svg slot="icon" 
        width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.3 0.7L12.1 4.5H0V5.5H12.1L8.3 9.3L9 10L14 5L9 0L8.3 0.7Z" fill="black"/>
    </svg>
</c2-button>
```
