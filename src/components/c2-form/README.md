# C2 Form
### Overview
C2 Form is form wrapper for the c2 form group and c2 form item components. It contains an unnamed slot for child components to be loaded

### Code snippet
```
<c2-form>
    <c2-form-group>
        some group items here...
    </c2-form-group>
</c2-form>
```

# C2 Form Group
### Overview
C2 Form Group is a wrapper for a form fieldset that includes a legend and content slot. This follows accessibility best practice.

### Slots
| Name        |
| ----------- |
| label  |
| content  |

### Code snippet
```
    <c2-form-group>
        <label slot="label">{{ labelName }}</label>
        <c2-text-input slot="content" :label="labelName" type="number" />
    </c2-form-group>
```

# C2 Form Item
### Overview
C2 Form Item is a wrapper that aligns form items to flex start using a column direction. No slot names or props included.

### Code snippet
```
    <c2-form-item>
        add content here...
    </c2-form-item>
```