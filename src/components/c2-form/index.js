import C2Form from './c2-form.vue';
import C2FormGroup from './c2-form-group.vue';
import C2FormItem from './c2-form-item.vue';

export { C2Form, C2FormGroup, C2FormItem };
