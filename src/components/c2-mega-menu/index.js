import C2MegaMenu from './c2-mega-menu.vue';
import C2MegaPrimaryNav from './c2-mega-primary-nav.vue';
import C2MegaSecondaryNav from './c2-mega-secondary-nav.vue';
import C2MegaTertiaryNav from './c2-mega-tertiary-nav.vue';
import C2MegaUtilityNav from './c2-mega-utility-nav.vue';

export { C2MegaMenu, C2MegaPrimaryNav, C2MegaSecondaryNav, C2MegaTertiaryNav, C2MegaUtilityNav };
