# C2 Modal
### Overview
Flexible modal that has built-in buttons, footers and customizable options

### Props
| Props                 | Type        | Default         |
| --------------------- | ----------- | --------------- |
| backgroundColor       | String      | rgba(0,0,0,0.7) |
| btnCancelClass        | String      |                 |
| btnCloseClass         | String      |                 |
| btnOpenClass          | String      |                 |
| btnSubmitClass        | String      |                 |
| forceClose            | Boolean     |                 |
| hasFooter             | Boolean     | true            |
| hasOpenBtn            | Boolean     | true            |
| maxWidth              | Number      | 1024            |
| showSubmitButton      | Boolean     | false           |
| disableSubmitButton   | Boolean     | false           |

### Slots
| Name                  |
| --------------------- |
| btn-open-text         |
| btn-open-icon         |
| btn-open-hover-icon   |
| title                 |
| btn-close-text        |
| btn-close-icon        |
| content               |
| btn-cancel-text       |
| btn-cancel-icon       |
| btn-cancel-hover-icon |
| btn-submit-text       |
| btn-submit-icon       |
| btn-submit-hover-icon |

### Code snippet
```
    <c2-modal
        ref="sharedModal"
        btn-open-class="btn-option body-two"
        :has-footer="false"
        :has-open-btn="false"
        :max-width="1920"
        @modalOpened="modalOpened"
        @modalClosed="modalClosed"
    >
        <span 
            slot="btn-close-text" 
            class="sr-only"
        >
            Close Modal
        </span>

        <span slot="btn-close-icon">
            <close-icon size="lg" />
        </span>

        <h2 slot="title">
            {{ title }}
        </h2>

        <div slot="content">
            {{ description }}
        </div>
    </c2-modal>
```
