# C2 Checkbox Input
### Overview
C2 checkbox input that creates a form input element.

### Props
| Props        | Type        |
| ------------ | ----------- |
| disabledClass  | String    |
| isDisabled  | Boolean      |
| label  | String            |
| modelValue  | Boolean      |
| trueValue  | Boolean       |
| falseValue  | Boolean      |
| value  | [String, Boolean] |

### Slots
| Name        |
| ----------- |
| checkbox-icon |

### Code snippet
```
<c2-checkbox-input
	:disabledClass="disabled"
	:isDisabled="false"
	:label="model.label"
	:modelValue="model.modelValue"
	:trueValue="model.trueValue"
	:falseValue="model.falseValue"
	:value="model.initalValue"
>
	<inline-svg slot="checkbox-icon" src="/Static/dist/svg/check-box-icon.svg" />
</c2-checkbox-input>
```
