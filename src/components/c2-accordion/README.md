# C2 Accordion Component
### Overview
C2 accordion that allows you to pass in an accordion title/content and loop through the c2 accordion items

### Props
| Props        | Type        |
| ------------ | ----------- |
| description  | String      |

### Slots
| Name        |
| ----------- |
| title       |
| content     |

### Code snippet
```
<c2-accordion>
    <div slot="title">Accordion Title</div>
    <template slot="content">
        <c2-accordion-item v-for="(accordionItem, index) in model.accordionItems" :key="`accordionItem-${index}`" :is-open="accordionItem.isOpen">
            <inline-svg slot="close-icon" src="/Static/dist/svg/chevron-down.svg" />
            <inline-svg slot="open-icon" src="/Static/dist/svg/chevron-up.svg" />
            <template slot="title" v-if="model.title">
                Some title text here
            </template>
            <rich-text slot="content" :html="accordionItem.desc"></rich-text>
        </c2-accordion-item>
    </template>
</c2-accordion>
```

# C2 Accordion Item Component
### Overview
C2 accordion item displays each item in the accordion.

### Props
| Props        | Type        |
| ------------ | ----------- |
| isDisabled   | Boolean      |
| isOpen       | Boolean      |
| activeClass  | String      |

### Slots
| Name        |
| ----------- |
| title       |
| open-icon   |
| close-icon  |

### Code snippet
```
<c2-accordion-item :is-open="accordionItem.isOpen">
    <inline-svg slot="close-icon" src="/Static/dist/svg/chevron-down.svg" />
    <inline-svg slot="open-icon" src="/Static/dist/svg/chevron-up.svg" />
    <template slot="title" v-if="model.title">
        Some title text here
    </template>
    <rich-text slot="content" :html="accordionItem.desc"></rich-text>
</c2-accordion-item>
```
