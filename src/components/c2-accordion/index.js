import C2Accordion from './c2-accordion.vue';
import C2AccordionItem from './c2-accordion-item.vue';

export { C2Accordion, C2AccordionItem };
