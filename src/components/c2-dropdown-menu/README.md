# C2 Dropdown Menu
### Overview
C2 Dropdown Menu creates a list of items from an array of options. This menu will be toggled from an icon button and displays the active name of a selected menu item.

### Props
| Props        | Type        |
| ------------ | ----------- |
| options      | Array      |
| firstActive  | Boolean      |
| value        | String || Number      |
| isClosed     | Boolean      |
| menuLabel    | String      |
| autoClose    | Boolean      |
| menuPosition | String      |

### Slots
| Name        |
| ----------- |
| dropdownIcon |

### Code snippet
```
    <c2-dropdown-menu
        :options="dropdownMenuOptions(viewModel.region.options, viewModel.region.placeholder)"
        :disabled="accountStatus"
        :has-error="invalidRegion"
        :class="{'error': invalidRegion}"
    >
        <chevron-down-icon
            slot="dropdownIcon"
        />
    </c2-dropdown-menu>
```
