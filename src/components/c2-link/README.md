# C2 Link
### Overview
C2 Link is a link that allows for the option to use the router or an anchor tag link. Attributes noreferrer nopener are applied to links that where assumeAbsoluteIsCrossOrigin is false and urls are absolute (automatically calculated)

### Props
| Props                         | Type        | Default     |
| ----------------------------- | ----------- | ----------- |
| assumeAbsoluteIsCrossOrigin   | Boolean     | true        |
| isDisabled                    | Boolean     | false       |
| href                          | String      |             |
| target                        | String      | _self       |
| useRouter                     | Boolean     | false       |

### Code snippet
```
    <c2-image
        :alt="Image Alt ttext"
        mobile-src="/Static/img/mobile.webp"
        src="/Static/img/desktop.webp"
        aspect-ratio="16/9"
    >
```
