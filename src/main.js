import * as components from './components';
import * as directives from './directives';

export default {
    install(Vue, options) {
        // components
        for (const componentName in components) {
            const component = components[componentName]
            Vue.component(component.name, component)
        }

        // directives
        for (const directiveName in directives) {
            const directive = directives[directiveName];
            Vue.directive(directive.name, directive);
        }
    }
};

// directives
import {
    C2MatchHeight,
    C2Debounce,
    C2LazyLoad,
    C2ClickOutside,
    C2ImageFallback,
    C2LineClamp,
	C2Resize,
    C2Money,
    C2Scroll,
    C2ScrollTo,
    C2ScrollLock,
    C2Focus,
    C2Blur,
	C2Touch
} from './directives';

export {
    C2MatchHeight,
    C2Debounce,
    C2LazyLoad,
    C2ClickOutside,
    C2ImageFallback,
    C2LineClamp,
	C2Resize,
    C2Money,
    C2Scroll,
    C2ScrollTo,
    C2ScrollLock,
    C2Focus,
    C2Blur,
	C2Touch
};

// components
import {
    C2Button,
    C2ButtonGroup,
    C2ButtonLink,
    C2Column,
    C2Container,
    C2Row,
    C2Tab,
    C2TabItem,
    C2Accordion,
    C2AccordionItem,
    C2Link,
    C2Image,
    C2WithRoot,
    C2List,
    C2ListItem,
    C2Card,
    C2Slider,
    C2MegaMenu,
    C2MegaTertiaryNav,
    C2MegaUtilityNav,
    C2MegaSecondaryNav,
    C2MegaPrimaryNav,
    C2HeroBanner,
    C2BackgroundColorHeroBanner,
    C2ImageHeroBanner,
    C2Form,
    C2FormGroup,
    C2FormItem,
    C2TextInput,
    C2DropdownMenu,
    C2SiteAlert,
    C2Breadcrumbs,
    C2BreadcrumbItem,
    C2CheckboxInput,
    C2PrimaryNav,
    C2StaticNav,
    C2Tag,
    C2Pagination,
    C2Video,
    C2DatePicker,
    C2Modal,
    C2Select,
    C2SelectOption,
    C2SelectOptgroup,
    C2LoadingBar,
    C2TextArea,
    C2FileUpload,
    C2RadioInput,
    C2ProductGallery
} from './components';

export {
    C2Button,
    C2ButtonGroup,
    C2ButtonLink,
    C2Column,
    C2Container,
    C2Row,
    C2Tab,
    C2TabItem,
    C2Accordion,
    C2AccordionItem,
    C2Link,
    C2Image,
    C2WithRoot,
    C2List,
    C2ListItem,
    C2Card,
    C2Slider,
    C2MegaMenu,
    C2MegaTertiaryNav,
    C2MegaUtilityNav,
    C2MegaSecondaryNav,
    C2MegaPrimaryNav,
    C2HeroBanner,
    C2BackgroundColorHeroBanner,
    C2ImageHeroBanner,
    C2Form,
    C2FormGroup,
    C2FormItem,
    C2TextInput,
    C2DropdownMenu,
    C2SiteAlert,
    C2Breadcrumbs,
    C2BreadcrumbItem,
    C2CheckboxInput,
    C2PrimaryNav,
    C2StaticNav,
    C2Tag,
    C2Pagination,
    C2Video,
    C2DatePicker,
    C2Modal,
    C2Select,
    C2SelectOption,
    C2SelectOptgroup,
    C2LoadingBar,
    C2TextArea,
    C2FileUpload,
    C2RadioInput,
    C2ProductGallery
};
