let handleOutsideClickMap = new Map();
const getNodeId = (node) => node.context._uid;

export const C2ClickOutside = {
    name: 'click-outside',
    bind(el, binding, node) {
        const handleOutsideClick = (e) => {
            e.stopPropagation();

            const handler = binding.value;

            if (!el.contains(e.target)) {
                handler();
            }
        };

        handleOutsideClickMap.set(getNodeId(node), handleOutsideClick);
        document.addEventListener("click", handleOutsideClick);
    },
    unbind(el, binding, node) {
        document.removeEventListener("click", handleOutsideClickMap.get(getNodeId(node)));
    }
};

export default C2ClickOutside;
