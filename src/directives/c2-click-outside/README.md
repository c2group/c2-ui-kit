# Click Outside Directive
Directive that allows you to call a method when a user clicks outside of am element
### Code snippet
```
<div v-click-outside="closeMenu"></div>
```
