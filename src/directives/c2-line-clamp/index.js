/* based on https://github.com/Frondor/vue-line-clamp
but without options */

const currentValueProp = 'vLineClampValue';

function defaultFallbackFunc(el, bindings, lines) {
    if (lines) {
        let lineHeight = parseInt(bindings.arg);
		const defaultLineHeight = 16;

        if (isNaN(lineHeight)) {
            console.warn(
                'line-height argument for vue-line-clamp must be a number (of pixels), falling back to 16px'
            );
            lineHeight = defaultLineHeight;
        }

        const maxHeight = lineHeight * lines;

        el.style.maxHeight = maxHeight ? maxHeight + 'px' : '';
        el.style.overflowX = 'hidden';
        el.style.lineHeight = lineHeight + 'px'; // to ensure consistency
    } else {
        el.style.maxHeight = el.style.overflowX = '';
    }
}

const truncateText = function(el, bindings, useFallbackFunc) {
    const lines = parseInt(bindings.value);

    if (isNaN(lines)) {
        console.error('Parameter for vue-line-clamp must be a number');
        return;
    } else if (lines !== el[currentValueProp]) {
        el[currentValueProp] = lines;

        if (useFallbackFunc) {
            useFallbackFunc(el, bindings, lines);
        } else {
            el.style.webkitLineClamp = lines ? lines : '';
        }
    }
};

const styles = `
      display: block;
      display: -webkit-box;
      -webkit-box-orient: vertical;
      overflow: hidden;
      word-break: break-all;
      text-overflow: ellipses;`;

const useFallbackFunc = 'webkitLineClamp' in document.body.style ? undefined : defaultFallbackFunc;

export const C2LineClamp = {
    name: 'line-clamp',
    bind(el) {
        el.style.cssText += styles;
    },
    inserted: (el, bindings) => truncateText(el, bindings, useFallbackFunc),
    updated: (el, bindings) => truncateText(el, bindings, useFallbackFunc),
    componentUpdated: (el, bindings) => truncateText(el, bindings, useFallbackFunc)
};

export default C2LineClamp;
