export const C2ScrollTo = {
    name: 'scrollTo',
    bind(el, binding) {

        const scrollTo = (e) => {
            e.preventDefault();
            const target = document.querySelector(binding.value.target);
            const offset = binding.value.offset;
            const headerFixed = binding.value.headerFixed;
            let scrollOffset = target.getBoundingClientRect().top + window.scrollY;

            if (offset) {

                scrollOffset -= offset;
            }

            if (headerFixed) {
                const offsetHeaderHeight = document.getElementsByTagName('header')[0].offsetHeight;

                scrollOffset -= offsetHeaderHeight;
            }

            if (target) {

                window.scrollTo({
                    top: scrollOffset,
                    behavior: 'smooth'
                });
            }
        }

        el.addEventListener('click', scrollTo);
    }
};

export default C2ScrollTo;
