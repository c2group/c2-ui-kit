# Scroll To Directive

### Overview
Gets target location from the top of the window and scrolls to it on click. If offset an offset number is provided it’ll add spacing from the top of the window in pixels. If the headerFixed value is true it’ll find the header element on the page and calculate the height of it then offset the value from the top of the window.

```
<button v-scrollTo="{
    target: '#item',
    offset: 100,
    headerFixed: true
}">Scroll To</button>
```
or
```
<button v-scrollTo="#item">Scroll To</button>
```