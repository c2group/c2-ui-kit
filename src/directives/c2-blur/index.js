const updateStyles = (el, binding) => {
    let isBlurred;
    let filter = 'blur(1.2px)';
    let opacity = 0.3; // symbol for decimal, e.g. European uses comma
    let transition = '0.3s ease opacity, 0.3s ease filter';
    
    if (binding.value && typeof binding.value === 'object') {

        if (binding.value.isBlurred) {
            isBlurred = binding.value.isBlurred;
        }
    
        if (binding.value.filter) {
            filter = binding.value.filter;
        }
    
        if (binding.value.opacity) {
            opacity = binding.value.opacity;
        }
    
        if (binding.value.transition) {
            transition = binding.value.transition;
        }

        if (!binding.value.isBlurred) {
            opacity = 1;
            filter = 'none';
        }
    } else if (typeof binding.value === 'boolean') {

        if (!binding.value) {
            opacity = 1;
            filter = 'none';
        }
    }

    el.style.filter = filter;
    el.style.opacity = opacity;
    el.style.transition = transition;
}

export const C2Blur = {
    name: 'blur',
    bind: (el, bindings) => updateStyles(el, bindings),
    updated: (el, bindings) => updateStyles(el, bindings),
    componentUpdated: (el, bindings) => updateStyles(el, bindings)
};

export default C2Blur;
