import debounce from '../c2-debounce/debounce'

export const C2Resize = {
    name: 'resize',
    bind(el, binding) {

		const timer = binding.arg;
        const debouncedFn = binding.value;

		if (timer) {
			window.onresize = debounce(() => {
				debouncedFn();
			}, timer)
		} else {
			window.onresize = () => {
				debouncedFn();
			}
		}
    }
};

export default C2Resize;
