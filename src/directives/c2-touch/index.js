export const C2Touch = {
    name: "touch",
    bind(el, binding) {
        const callback = binding.value;
        const eventType = binding.arg; // options are: move, start, end, left, right, up, down

        const touchsurface = el;
        let swipedir = "";
        let startX = 0;
        let startY = 0;
        let distX = 0;
        let distY = 0;
        const threshold = 150; //required min distance traveled to be considered swipe; might want to increase
        const restraint = 100; // maximum distance allowed at the same time in perpendicular direction
        const allowedTime = 200; // maximum time allowed to travel that distance; might want to increase
        let elapsedTime = 0;
        let startTime = 0;

        const handleswipe = function() {
            if (swipedir === "right" && eventType === "right") {
                callback();
            }
            if (swipedir === "left" && eventType === "left") {
                callback();
            }
            if (swipedir === "up" && eventType === "up") {
                callback();
            }
            if (swipedir === "down" && eventType === "down") {
                callback();
            }
        };

        touchsurface.addEventListener(
            "touchstart",
            function(e) {
                var touchobj = e.changedTouches[0];
                swipedir = "none";
                distX = 0;
                distY = 0;
                startX = touchobj.pageX;
                startY = touchobj.pageY;
                startTime = new Date().getTime(); // record time when finger first makes contact with surface
                e.preventDefault();

                if (eventType === "start") {
                    callback();
                }
            },
            false
        );

        touchsurface.addEventListener(
            "touchmove",
            function(e) {
                e.preventDefault(); // prevent scrolling when inside DIV

                if (eventType === "move") {
                    callback();
                }
            },
            false
        );

        touchsurface.addEventListener(
            "touchend",
            function(e) {
                var touchobj = e.changedTouches[0];
                distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
                distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
                elapsedTime = new Date().getTime() - startTime; // get time elapsed
                if (elapsedTime <= allowedTime) {
                    // first condition for swipe met
                    if (
                        Math.abs(distX) >= threshold &&
                        Math.abs(distY) <= restraint
                    ) {
                        // 2nd condition for horizontal swipe met
                        swipedir = distX < 0 ? "left" : "right"; // if dist traveled is negative, it indicates left swipe
                    } else if (
                        Math.abs(distY) >= threshold &&
                        Math.abs(distX) <= restraint
                    ) {
                        // 2nd condition for vertical swipe met
                        swipedir = distY < 0 ? "up" : "down"; // if dist traveled is negative, it indicates up swipe
                    }
                }
                handleswipe(swipedir);
                e.preventDefault();

                if (eventType === "end") {
                    callback();
                }
            },
            false
        );
    }
};

export default C2Touch;
