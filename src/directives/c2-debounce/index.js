import debounce from './debounce'

export default {
    name: 'debounce',
    bind(el, binding) {
        const timer = binding.arg;
        const debouncedFn = binding.value;

        el.oninput = debounce(({ target }) => {
            debouncedFn(target.value)
        }, timer)
    }
}
