export default (fn, wait) => {
    let timeout = null
    const DEFAULT_WAIT = 300;
    const timer = wait || DEFAULT_WAIT;

    return function() {
        const context = this;
        const args = arguments;
        const later = function() {
            timeout = null
            fn.apply(context, args)
        }

        clearTimeout(timeout);
        timeout = setTimeout(later, timer);

        if (!timeout) {
            fn.apply(context, args)
        }
    };
};
