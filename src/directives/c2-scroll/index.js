export const C2Scroll = {
    name: 'scroll',
    bind(el, binding) {

        if ('IntersectionObserver' in window) {

            const observer = new IntersectionObserver((entry) => {
                const handler = binding.value;

                entry.forEach(item => {

                    if (item.isIntersecting) {
                        handler();
                        removeListener();
                    }
                });
            });
            
            const removeListener = () => {
                observer.unobserve(el);
            }

            observer.observe(el);
        }
    }
};

export default C2Scroll;
