# Scroll To Directive

### Overview
Uses intersection observer to detect if element is in the window view. If the element is in the window view the callback method is called and the observer is removed from the dom.

```
<div v-scroll="callbackMethod"> Let's get it</div>
```