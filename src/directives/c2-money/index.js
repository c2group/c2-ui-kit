import accounting from './accounting';

export const C2Money = {
    name: 'money',
    bind(el, binding) {

		let symbol = '$';
		let decimal = '.'; // symbol for decimal, e.g. European uses comma
		let thousand = ','; // symbol for thousand notation, e.g. European uses decimal
		let precision = 2; // numbers after decimal
		let format = '%s%v'; // controls symbol position
		
		if (binding.value) {
			if (binding.value.symbol) {
				symbol = binding.value.symbol;
			}

			if (binding.value.decimal) {
				decimal = binding.value.decimal;
			}

			if (binding.value.thousand) {
				thousand = binding.value.thousand;
			}

			if (binding.value.precision) {
				precision = binding.value.precision;
			}

			if (binding.value.format) {
				format = binding.value.format;
			}
		}

		function updateValue(e) {
			const value = e.target.value;
			const result = accounting.formatMoney(value, symbol, decimal, thousand, precision, format);
			el.value = result;
		}

		el.addEventListener('input', updateValue);
    }
};

export default C2Money;
