# Money Directive

```
<input
    type="text"
    v-money="{
        symbol: '£',
        decimal: '.',
        thousand: '.',
        precision: 2,
        format: '%s%v',
    }"
/>
```