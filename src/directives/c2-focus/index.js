export const C2Focus = {
    name: 'focus',
	inserted: function(el, binding) {
		if (binding.value) {
			el.focus();
		}
	},
	componentUpdated: function (el, binding) {
		if (binding.value) {
			el.focus();
		}
	}
};

export default C2Focus;
