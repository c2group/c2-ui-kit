import lazyImageObserver from './lazyObserver'
import constants from './lazyLoadConstants'

export const C2LazyLoad = {
    name: 'lazyload',
    bind (el) {
        if ('IntersectionObserver' in window) {
            lazyImageObserver.observe(el)
        }
    },
    componentUpdated (el) {
        // when image changed
        // expecting has been loaded image before
        if ('IntersectionObserver' in window) {
            if (el.classList.contains(constants._V_LOADED)) {
                lazyImageObserver.observe(el)
            }
        }
    }
};

export default C2LazyLoad;
