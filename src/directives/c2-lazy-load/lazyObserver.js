import constants from './lazyLoadConstants';

let lazyImageObserver = null;

const clearDataSrc = (lazyImage, stateClass) => {
    lazyImage.classList.add(stateClass);

    lazyImage.removeAttribute('data-src');
    lazyImage.removeAttribute('data-err');
}

if ('IntersectionObserver' in window) {
    lazyImageObserver = new IntersectionObserver(function (entries) {
        entries.forEach(function (entry) {
            if (!entry.isIntersecting) {
                return;
            }

            const lazyImage = entry.target;
            lazyImage.classList.add(constants._V_LOADING);

            const dataSrc = lazyImage.dataset.src;
            const dataErr = lazyImage.dataset.err;

            if (!dataSrc && !dataErr) {
                lazyImage.classList.remove(constants._V_LOADING);
                lazyImageObserver.unobserve(lazyImage);
                return;
            } else if (!dataSrc && dataErr){
                lazyImage.classList.remove(constants._V_LOADING);
                lazyImage.src = dataErr;
                clearDataSrc(lazyImage, constants._V_ERROR);
                lazyImageObserver.unobserve(lazyImage);
                return;
            }

            var newImage = new Image();
            newImage.src = dataSrc;
            // when success
            newImage.onload = function () {
                lazyImage.classList.remove(constants._V_LOADING);

                if (dataSrc) {
                    lazyImage.src = dataSrc;
                    clearDataSrc(lazyImage, constants._V_LOADED);
                }
            };

            // when error
            newImage.onerror = function () {
                lazyImage.classList.remove(constants._V_LOADING);

                if (dataErr) {
                    lazyImage.src = dataErr;
                    clearDataSrc(lazyImage, constants._V_ERROR);
                }
            };

            lazyImageObserver.unobserve(lazyImage);
        });
    });
}

export default lazyImageObserver;
