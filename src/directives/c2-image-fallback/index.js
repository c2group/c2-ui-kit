export const C2ImageFallback = {
    name: 'image-fallback',
    bind(el, binding) {
        el.onerror = function () {
            const fallbackImageSrc = binding.value.src;
            const fallbackImageAlt = binding.value.alt;
            
            el.src = fallbackImageSrc;
            el.alt = fallbackImageAlt;
        }
    }
};

export default C2ImageFallback;
