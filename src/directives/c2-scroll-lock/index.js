const body = document.getElementsByTagName('body')[0];

const toggleScrollLock = (locked) => {

    if (locked) {
        body.style.overflow = 'hidden';
    } else {
        body.style.overflow = '';
    }
}

export const C2ScrollLock = {
    name: 'scroll-lock',
    bind:  (el, binding) => toggleScrollLock(binding.value),
    unbind:  () => toggleScrollLock()
};

export default C2ScrollLock;