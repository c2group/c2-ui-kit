import Vue from 'vue';
import Router from 'vue-router';
import Intro from './components/Intro.vue';
import JonPlayground from './components/JonPlayground.vue';
import RyanPlayground from './components/RyanPlayground.vue';
import VincePlayground from './components/VincePlayground.vue';
import MickPlayground from './components/MickPlayground.vue';
import GenevievePlayground from './components/GenevievePlayground.vue';
import MikePlayground from './components/MikePlayground.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: Intro
        },
        {
            path: '/jonplayground/',
            name: 'jonplayground',
            component: JonPlayground
        },
        {
            path: '/ryanplayground/',
            name: 'ryanplayground',
            component: RyanPlayground
        },
        {
            path: '/vinceplayground/',
            name: 'vinceplayground',
            component: VincePlayground
        },
        {
            path: '/mickplayground/',
            name: 'mickplayground',
            component: MickPlayground
        },
        {
            path: '/genevieveplayground/',
            name: 'genevieveplayground',
            component: GenevievePlayground
        },
        {
            path: '/mikeplayground/',
            name: 'mikeplayground',
            component: MikePlayground
        },
    ]
});
