import Vue from 'vue';
import router from './router';
import App from './App.vue';
import C2UiKit from '../dist/c2-ui-kit.esm.js';

Vue.config.productionTip = false;
Vue.use(C2UiKit);

new Vue({
    router,
    render: (h) => h(App)
}).$mount('#app');
