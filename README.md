# C2 UI Kit

Works with node version 12.18.3

Startup commands:
- npm install (first time)
- npm run build
- npm run serve (starts server)

Notes:
- components should be built to accessibility standards
- prefix components with "c2-" unless third-party component
- prefix with "_" if sub-component
- follow existing components for structuring JS

To publish new package version to npm:
- merge develop into master
- checkout master branch
- pull latest
- npm login
- npm version patch (increments package version)
- npm publish
- git push (pushes new version back to master)