module.exports = {
  "root": true,
  "env": {
    "node": true
  },
'plugins': [
    'vuejs-accessibility'
],
  "extends": [
    "plugin:vue/essential",
      'plugin:vuejs-accessibility/recommended',
      'plugin:vue/essential',
      'plugin:vue/strongly-recommended',
      'plugin:vue/recommended',
    "eslint:recommended"
  ],

  "parserOptions": {
    "parser": "babel-eslint"
  }
};
